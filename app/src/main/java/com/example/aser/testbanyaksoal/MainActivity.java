package com.example.aser.testbanyaksoal;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity {

    public static int noSoal = 0;
    private TextView pertanyaan;
    private TextView jawab_a;
    private TextView jawab_b;
    private TextView jawab_c;
    private Button btn_a;
    private Button btn_b;
    private Button btn_c;
    private List data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        generateField();
        dataArray();

        btn_a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.noSoal += 1;
                setSoal();
            }
        });

        this.setSoal();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setSoal(){
        int[] arrayIsiData = (int[]) data.get(MainActivity.noSoal);
        pertanyaan.setText(arrayIsiData[0]);
        jawab_a.setText(arrayIsiData[1]);
        jawab_b.setText(arrayIsiData[2]);
        jawab_c.setText(arrayIsiData[3]);
    }

    private void generateField(){
        pertanyaan = (TextView) findViewById(R.id.txt_pertanyaan);
        jawab_a = (TextView) findViewById(R.id.txt_jawaban_a);
        jawab_b = (TextView) findViewById(R.id.txt_jawaban_b);
        jawab_c = (TextView) findViewById(R.id.txt_jawaban_c);

        btn_a = (Button) findViewById(R.id.btn_a);
        btn_b = (Button) findViewById(R.id.btn_b);
        btn_c = (Button) findViewById(R.id.btn_c);
    }

    private void dataArray(){
        data = new ArrayList();
        int[] dataTanya1 = new int[4];
        dataTanya1[0] = R.string.app_str_pertanyaan_1;
        dataTanya1[1] = R.string.app_str_jawaban_1a;
        dataTanya1[2] = R.string.app_str_jawaban_1b;
        dataTanya1[3] = R.string.app_str_jawaban_1c;
        data.add(dataTanya1);
        int[] dataTanya2 = new int[4];
        dataTanya2[0] = R.string.app_str_pertanyaan_2;
        dataTanya2[1] = R.string.app_str_jawaban_2a;
        dataTanya2[2] = R.string.app_str_jawaban_2b;
        dataTanya2[3] = R.string.app_str_jawaban_2c;
        data.add(dataTanya2);
        int[] dataTanya3 = new int[4];
        dataTanya3[0] = R.string.app_str_pertanyaan_3;
        dataTanya3[1] = R.string.app_str_jawaban_3a;
        dataTanya3[2] = R.string.app_str_jawaban_3b;
        dataTanya3[3] = R.string.app_str_jawaban_3c;
        data.add(dataTanya3);
        int[] dataTanya4 = new int[4];
        dataTanya4[0] = R.string.app_str_pertanyaan_4;
        dataTanya4[1] = R.string.app_str_jawaban_4a;
        dataTanya4[2] = R.string.app_str_jawaban_4b;
        dataTanya4[3] = R.string.app_str_jawaban_4c;
        data.add(dataTanya4);
        int[] dataTanya5 = new int[4];
        dataTanya5[0] = R.string.app_str_pertanyaan_5;
        dataTanya5[1] = R.string.app_str_jawaban_5a;
        dataTanya5[2] = R.string.app_str_jawaban_5b;
        dataTanya5[3] = R.string.app_str_jawaban_5c;
        data.add(dataTanya5);
        int[] dataTanya6 = new int[4];
        dataTanya6[0] = R.string.app_str_pertanyaan_6;
        dataTanya6[1] = R.string.app_str_jawaban_6a;
        dataTanya6[2] = R.string.app_str_jawaban_6b;
        dataTanya6[3] = R.string.app_str_jawaban_6c;
        data.add(dataTanya6);

    }

    @Override
    public void onBackPressed() {
        MainActivity.noSoal -= 1;
        setSoal();
    }


}
